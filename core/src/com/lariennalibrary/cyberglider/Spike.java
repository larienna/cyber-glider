package com.lariennalibrary.cyberglider;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Spike extends AbstractGameObject 
{
  private TextureRegion regSpike;
  //public boolean collected;

  public Spike () 
  {
    init();
  }

  private void init () 
  {
    dimension.set(1.0f, 1.0f);
    regSpike = Assets.instance.spike.spike;
    // Set bounding box for collision detection
    bounds.set(0.2f, 0.2f, dimension.x - 0.2f, dimension.y - 0.2f);
    origin.set ( 0.5f, 0.5f);
    //collected = false;
    //rotation = 90.0f;
  }

  @Override
  public void update (float deltaTime)
  {
	  // Rotate sprite by 90 degrees per second
	    rotation += 90 * deltaTime;
	    // Wrap around at 360 degrees
	    rotation %= 360;
	    
	  super.update(deltaTime);  
  }
  
  public void render (SpriteBatch batch) 
  {
    //if (collected) return;

    TextureRegion reg = null;
    reg = regSpike;
    batch.draw(reg.getTexture(),position.x, position.y,origin.x, origin.y,dimension.x, dimension.y,scale.x, scale.y,rotation,reg.getRegionX(), reg.getRegionY(),reg.getRegionWidth(), reg.getRegionHeight(),false, false);
  }

  /*public int getScore() 
  {
    return ( 1 );
  }*/
}
