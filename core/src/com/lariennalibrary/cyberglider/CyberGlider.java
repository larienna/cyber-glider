package com.lariennalibrary.cyberglider;



/*import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;*/



import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;


public class CyberGlider extends Game 
{
  @Override
  public void create () 
  {
    // Set Libgdx log level     Gdx.app.setLogLevel(Application.LOG_DEBUG);
    // Load assets
    Assets.instance.init(new AssetManager());
    // Start game at menu screen
    setScreen(new MenuScreen(this));
  }
}
