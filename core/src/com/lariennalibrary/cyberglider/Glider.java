package com.lariennalibrary.cyberglider;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;


public class Glider extends AbstractGameObject {
  public static final String TAG = Glider.class.getName();

  //private final float JUMP_TIME_MAX = 0.3f;
  //private final float JUMP_TIME_MIN = 0.1f;
  //private final float JUMP_TIME_OFFSET_FLYING =JUMP_TIME_MAX - 0.018f;

  /*public enum VIEW_DIRECTION 
  { UP, DOWN }*/
  
  /*public enum JUMP_STATE {
    GROUNDED, FALLING, JUMP_RISING, JUMP_FALLING
  }*/

  private TextureRegion regGlider;
  //public VIEW_DIRECTION viewDirection;
  //public float timeJumping;
  //public JUMP_STATE jumpState;
  //public boolean hasFeatherPowerup;
  //public float timeLeftFeatherPowerup;

  public Glider () 
  {
    init();
  }

  public void init () 
  {
	  dimension.set(1, 1);
	  regGlider = Assets.instance.glider.glider;
	  // Center image on game object
	  origin.set(dimension.x / 2, dimension.y / 2);
	  // Bounding box for collision detection
	  bounds.set(0.20f, 0.20f, dimension.x - 0.20f, dimension.y - 0.20f);
	  // Set physics values
	  terminalVelocity.set(8.0f, 4.0f);
	  friction.set(0.5f, 0.0f);
	  acceleration.set(0.0f, 0.0f);
	  // View direction
	  //viewDirection = VIEW_DIRECTION.RIGHT;
	  // Jump state
	  //jumpState = JUMP_STATE.FALLING;
	  //timeJumping = 0;
	  // Power-ups
	  //hasFeatherPowerup = false;
	  //timeLeftFeatherPowerup = 0;  
  }
  
  @Override
  public void update ( float deltaTime)
  {
	  //Gdx.app.log( "Glider.update", "Acceleration = " + acceleration.x 
	//		  + "Friction = " + friction.x + "Velocity = " + velocity.x);
	  
	  super.update(deltaTime);
	  
	  position.x = MathUtils.clamp(position.x, 0, 11);
	  if ( position.x == 0 || position.x == 11 )
	  {
		  velocity.x = 0;
		  acceleration.x = 0;
	  }
	  if ( position.y >= (100 ))
	  {
		  velocity.x = 0;
		  acceleration.x = 0;
	  }
  }
  
  @Override
  public void render (SpriteBatch batch) 
  {
    TextureRegion reg = null;

    // Set special color when game object has a feather power-up
    //if (hasFeatherPowerup)
    //batch.setColor(1.0f, 0.8f, 0.0f, 1.0f);
    // Draw image
    reg = regGlider;
    batch.draw(reg.getTexture(),
               position.x, position.y,
               origin.x, origin.y,
               dimension.x, dimension.y,
               scale.x, scale.y,
               rotation,
               reg.getRegionX(), reg.getRegionY(),
               reg.getRegionWidth(), reg.getRegionHeight(),
               false, false);
    
    // Reset color to white
    //batch.setColor(1, 1, 1, 1);
  }
  //public void setJumping (boolean jumpKeyPressed) {};
  //public void setFeatherPowerup (boolean pickedUp) {};
  //public boolean hasFeatherPowerup () {};
}
