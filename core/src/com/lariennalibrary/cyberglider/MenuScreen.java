package com.lariennalibrary.cyberglider;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class MenuScreen extends AbstractGameScreen 
{
  private static final String TAG = MenuScreen.class.getName();
  
  private static SpriteBatch tmpbatch = new SpriteBatch();
  private OrthographicCamera camera;

  public MenuScreen (Game game) 
  {
	  super(game);
	  camera = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH,
              Constants.VIEWPORT_GUI_HEIGHT);
	    camera.position.set(0, 0, 0);
	    camera.setToOrtho(true); // flip y-axis
	    camera.update();
	  
	 
	  
    
  }

  @Override
  public void render (float deltaTime) 
  {
    Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    
    tmpbatch.setProjectionMatrix(camera.combined); 
    
    float x = 50;
    float y = 200;
    tmpbatch.begin();
    //tmpbatch.draw(Assets.instance.data.data,
    //    x, y, 50, 50, 100, 100, 0.35f, -0.35f, 0);
    Assets.instance.fonts.defaultBig.draw(tmpbatch,
        "Cyber Glider",
        x , y);
    y += 32;
    Assets.instance.fonts.defaultBig.draw(tmpbatch,
            "Touch or Click to start",
            x , y );
    
    y += 64;
    Assets.instance.fonts.defaultBig.draw(tmpbatch,
            "Collect Coins \nand avoid spikes",
            x , y );
    
    y += 96;
    Assets.instance.fonts.defaultBig.draw(tmpbatch,
            "Move with LEFT-RIGHT key \nor touch/click left-right of screen",
            x , y );
    
    
    tmpbatch.end();
    
    if(Gdx.input.isTouched())
      game.setScreen(new GameScreen(game));
  }

  @Override public void resize (int width, int height) { }
  @Override public void show () { }
  @Override public void hide () { }
  @Override public void pause () { }
}