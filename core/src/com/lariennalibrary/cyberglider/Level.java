package com.lariennalibrary.cyberglider;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class Level 
{
  public static final String TAG = Level.class.getName();
   public Background bg;
   
   public Glider glider;
   public Array<DataCoin> datacoins;
   public Array<Spike> spikes;
   
   // needs to add spikes and coins
   
   public enum BLOCK_TYPE 
   {
	    EMPTY(0, 0, 0),                   // black
	    DATA(0, 255, 0),                  // green
	    SPIKE(255, 0, 0),                 // red
	    PLAYER(255, 255, 0);              // yellow

	    private int color;

	    private BLOCK_TYPE (int r, int g, int b) 
	    {
	      color = r << 24 | g << 16 | b << 8 | 0xff;
	    }

	    public boolean sameColor (int color) 
	    {
	      return this.color == color;
	    }

	    public int getColor () 
	    {
	      return color;
	    }
	  }
  
  public Level (String filename) 
  {
	    init(filename);
  }

  private void init (String filename) 
  {
	  bg = new Background ( 100 );
	  
	  glider = new Glider();
	  datacoins = new Array<DataCoin>();
	  spikes = new Array<Spike>();
	  
	// load image file that represents the level data
	    //Gdx.app.debug(TAG, "Loading level '" + filename + "' ");
	    Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));
	    // scan pixels from top-left to bottom-right
	    int lastPixel = -1;
	    for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
	      for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
	        AbstractGameObject obj = null;
	        float offsetHeight = 0;
	        // height grows from bottom to top
	        float baseHeight = pixmap.getHeight() - pixelY;
	        // get color of current pixel as 32-bit RGBA value
	        int currentPixel = pixmap.getPixel(pixelX, pixelY);
	        // find matching color value to identify block type at (x,y)
	        // point and create the corresponding game object if there is
	        // a match
	        // empty space
	        if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) 
	        {
	          // do nothing
	        }
	        
	        // player spawn point
	        else if
	            (BLOCK_TYPE.PLAYER.sameColor(currentPixel)) 
	        {
	        	//obj = new Glider();
	            offsetHeight = -0.5f;
	            glider.position.set(pixelX,baseHeight * glider.dimension.y+ offsetHeight);
	            //glider = (Glider)obj;
	        }
	        // spike
	        else if (BLOCK_TYPE.SPIKE.sameColor(currentPixel)) 
	        {
	        	 obj = new Spike();
	             offsetHeight = -0.5f;
	             obj.position.set(pixelX,baseHeight * obj.dimension.y+ offsetHeight);
	             spikes.add((Spike)obj);
	        }
	        // data
	        else if (BLOCK_TYPE.DATA.sameColor(currentPixel)) 
	        {
	        	 obj = new DataCoin();
	             offsetHeight = -0.5f;
	             obj.position.set(pixelX,baseHeight * obj.dimension.y+ offsetHeight);
	             datacoins.add((DataCoin)obj);
	        }
	        // unknown object/pixel color
	        else {
	          int r = 0xff & (currentPixel >>> 24); //red color channel
	          int g = 0xff & (currentPixel >>> 16); //green color channel
	          int b = 0xff & (currentPixel >>> 8);  //blue color channel
	          int a = 0xff & currentPixel;          //alpha channel
	          Gdx.app.error(TAG, "Unknown object at x<" + pixelX
	                                           + "> y<" + pixelY
	                                          + ">: r<" + r
	                                           + "> g<" + g
	                                           + "> b<" + b
	                                           + "> a<" + a + ">");
	        }
	        lastPixel = currentPixel;
	      }
	    }	  

	    // free memory
	    pixmap.dispose();
	    Gdx.app.debug(TAG, "level '" + filename + "' loaded");
  }
	  
  public void render (SpriteBatch batch) 
  {
	  bg.render( batch );
	  
	// Draw Gold Coins
	  for (DataCoin dataCoin : datacoins)
	    dataCoin.render(batch);
	  // Draw Feathers
	  for (Spike spike : spikes)
	    spike.render(batch);
	  // Draw Player Character
	  glider.render(batch);
  }
 
  
  public void update (float deltaTime) 
  {
	  glider.update(deltaTime);
	  //for(Rock rock : rocks)
	  //  rock.update(deltaTime);
	  for(DataCoin dataCoin : datacoins)
	    dataCoin.update(deltaTime);
	  for(Spike spike : spikes)
	    spike.update(deltaTime);
	  
	}
}