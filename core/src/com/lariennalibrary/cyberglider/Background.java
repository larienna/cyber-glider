package com.lariennalibrary.cyberglider;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Background extends AbstractGameObject
{
	public static final String TAG = Assets.class.getName();
	private int length;
	private TextureRegion bgtexture;
	private int nb_lines;
	
	public Background ( int length )
	{
		this.length = length;
		init();
	}
	
	private void init ()
	{
		dimension.set( 6.0f, 3.0f);
		nb_lines = length / 3;
		nb_lines += 2;
		bgtexture = Assets.instance.background.background;
	}
	
	@Override
	public void render (SpriteBatch batch)
	{
		float yoff = 0;//bgtexture.getRegionWidth();
		float xoff = 6;
		
		for ( int offset = 0; offset < nb_lines; offset++ )
		{	
			yoff = offset * 3;//bgtexture.getRegionHeight();
			//Gdx.app.debug(TAG, "offset: " + offset + " xoff=" + xoff );
			
			batch.draw ( bgtexture, 0, yoff, dimension.x, dimension.y );
			batch.draw ( bgtexture, xoff, yoff, dimension.x, dimension.y );
			/*batch.draw(bgtexture.getTexture(),
                origin.x + xoff, origin.y ,
                origin.x, origin.y,
                dimension.x, dimension.y,
                scale.x, scale.y,
                rotation,
                bgtexture.getRegionX(), bgtexture.getRegionY(),
                bgtexture.getRegionWidth(), bgtexture.getRegionHeight(),
                false, false);*/

			//batch.draw ( bgtexture.getTexture(), xoff, yoff);

			
			/*batch.draw(bgtexture.getTexture(),
	                origin.x + xoff, origin.y + yoff,
	                origin.x, origin.y,
	                dimension.x, dimension.y,
	                scale.x, scale.y,
	                rotation,
	                bgtexture.getRegionX(), bgtexture.getRegionY(),
	                bgtexture.getRegionWidth(), bgtexture.getRegionHeight(),
	                false, false);*/
			
			
		}
		/*public void draw (Texture texture,
                  float x, float y,
                  float originX, float originY,
                  float width, float height,
                  float scaleX, float scaleY,
                  float rotation,
                  int srcX, int srcY,
                  int srcWidth, int srcHeight,
                  boolean flipX, boolean flipY);*/
	}
}
