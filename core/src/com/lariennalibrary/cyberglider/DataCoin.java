package com.lariennalibrary.cyberglider;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class DataCoin extends AbstractGameObject 
{
  private TextureRegion regDataCoin;
  public boolean collected;

  public DataCoin () 
  {
    init();
  }

  private void init () 
  {
    dimension.set(1.0f, 1.0f);
    regDataCoin = Assets.instance.data.data;
    // Set bounding box for collision detection
    bounds.set(0, 0, dimension.x, dimension.y);
    collected = false;
  }

  public void render (SpriteBatch batch) 
  {
    if (collected) return;

    TextureRegion reg = null;
    reg = regDataCoin;
    batch.draw(reg.getTexture(),position.x, position.y,origin.x, origin.y,dimension.x, dimension.y,scale.x, scale.y,rotation,reg.getRegionX(), reg.getRegionY(),reg.getRegionWidth(), reg.getRegionHeight(),false, false);
  }

  public int getScore() 
  {
    return ( 1 );
  }
}
