package com.lariennalibrary.cyberglider;

public class Constants 
{
	public final static float VIEWPORT_WIDTH = 12.0f;
	public final static float VIEWPORT_HEIGHT = 18.0f;
	public static final String TEXTURE_ATLAS_OBJECTS =
		    "images/cyberglider.pack";
	 // Location of image file for level 01
	public static final String LEVEL_01 = "levels/level_01.png";
	// Amount of extra lives at level start
	public static final int LIVES_START = 3;
	// GUI Width
	public static final float VIEWPORT_GUI_WIDTH = 480.0f;
	  // GUI Height
	public static final float VIEWPORT_GUI_HEIGHT = 800.0f;
	// Delay after game over
	public static final float TIME_DELAY_GAME_OVER = 3;
	
}
