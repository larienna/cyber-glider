package com.lariennalibrary.cyberglider;



import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends AbstractGameScreen 
{
  private static final String TAG = GameScreen.class.getName();

  private WorldController worldController;
  private WorldRenderer worldRenderer;

  private boolean paused;
  

  public GameScreen (Game game) 
  {
    super(game);
  }

  @Override
  public void render (float deltaTime) 
  {
	if ( worldController.exitgame == false )
	{
		
	
		// Do not update game world when paused.
		if (paused == false) 
		{
			// Update game world by the time that has passed
			// since last rendered frame.
			worldController.update(deltaTime);
		}
		// Sets the clear screen color to: Cornflower Blue
		Gdx.gl.glClearColor(0x00/255.0f, 0x10/255.0f, 0x40/255.0f, 0xff/255.0f);
		// Clears the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// Render game world to screen
		worldRenderer.render();
  	}
	else
	{
		game.setScreen(new MenuScreen(game));
	}
  }
  @Override
  public void resize (int width, int height) 
  {
    worldRenderer.resize(width, height);
  }

  @Override
  public void show () 
  {
    worldController = new WorldController(game);
    worldRenderer = new WorldRenderer(worldController);
    Gdx.input.setCatchBackKey(true);
  }

  @Override
  public void hide () 
  {
    worldRenderer.dispose();
    worldController.dispose();
    Gdx.input.setCatchBackKey(false);
  }

  @Override
  public void pause () 
  {
    paused = true;
  }

  @Override
  public void resume () 
  {
    super.resume();
    // Only called on Android!
    paused = false;
  }
}