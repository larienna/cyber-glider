package com.lariennalibrary.cyberglider;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.lariennalibrary.cyberglider.Constants;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets implements Disposable, AssetErrorListener 
{
  public static final String TAG = Assets.class.getName();
  public static final Assets instance = new Assets();
  private AssetManager assetManager;
  
  public AssetGlider glider;
  public AssetData data;
  public AssetSpike spike;
  public AssetBackground background;

  // singleton: prevent instantiation from other classes
  private Assets () {}

  public void init (AssetManager assetManager) {
    this.assetManager = assetManager;
    // set asset manager error handler
    assetManager.setErrorListener(this);
    // load texture atlas
    assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS,TextureAtlas.class);
    // start loading assets and wait until finished
    assetManager.finishLoading();
    Gdx.app.debug(TAG, "# of assets loaded: "+ assetManager.getAssetNames().size);
    for (String a : assetManager.getAssetNames())
      Gdx.app.debug(TAG, "asset: " + a);
    
    TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);
    
 // enable texture filtering for pixel smoothing
    for (Texture t : atlas.getTextures())
      t.setFilter(TextureFilter.Linear, TextureFilter.Linear);

    // create game resource objects
    fonts = new AssetFonts();
    glider = new AssetGlider(atlas);
    data = new AssetData(atlas);
    spike = new AssetSpike(atlas);
    background = new AssetBackground(atlas);
   
  }

  @Override
  public void dispose () {
    assetManager.dispose();
    fonts.defaultSmall.dispose();
    fonts.defaultNormal.dispose();
    fonts.defaultBig.dispose();
  }

  
  @Override
  public void error (AssetDescriptor asset,  java.lang.Throwable throwable)
  {
    Gdx.app.error(TAG, "Couldn't load asset '"+ asset.fileName + "'", (Exception)throwable);
  }
  
  // -------------------------- Asset inner Class -----------------
  
  public class AssetGlider 
  {
	  public final AtlasRegion glider;
	  
	  public AssetGlider (TextureAtlas atlas) 
	  {
	    glider = atlas.findRegion("SpriteGlider");
	  }
  }
  
  public class AssetData 
  {
	  public final AtlasRegion data;
	  public AssetData (TextureAtlas atlas) 
	  {
	    data = atlas.findRegion("SpriteData");
	  }
  }
  
  public class AssetSpike 
  {
	  public final AtlasRegion spike;
	  public AssetSpike (TextureAtlas atlas) 
	  {
	    spike = atlas.findRegion("SpriteSpike");
	  }
  }
  
  public class AssetBackground 
  {
	  public final AtlasRegion background;
	  
	  public AssetBackground (TextureAtlas atlas) 
	  {
	    background = atlas.findRegion("z_Background");
	  }
	}
  
  public AssetFonts fonts;

  public class AssetFonts 
  {
    public final BitmapFont defaultSmall;
    public final BitmapFont defaultNormal;
    public final BitmapFont defaultBig;

    public AssetFonts () 
    {
      // create three fonts using Libgdx's 15px bitmap font
      defaultSmall = new BitmapFont(
          Gdx.files.internal("images/arial-15.fnt"), true);
      defaultNormal = new BitmapFont(
          Gdx.files.internal("images/arial-15.fnt"), true);
      defaultBig = new BitmapFont(
          Gdx.files.internal("images/arial-15.fnt"), true);
      // set font sizes
      // for some reason set scale on font does not exists anymore
      //defaultSmall.setScale(0.75f);
      //defaultNormal.setScale(1.0f);
      //defaultBig.setScale(2.0f);
      // enable linear texture filtering for smooth fonts
      defaultSmall.getRegion().getTexture().setFilter(
          TextureFilter.Linear, TextureFilter.Linear);
      defaultNormal.getRegion().getTexture().setFilter(
          TextureFilter.Linear, TextureFilter.Linear);
      defaultBig.getRegion().getTexture().setFilter(
          TextureFilter.Linear, TextureFilter.Linear);
    }
  }
  
}