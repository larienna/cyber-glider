package com.lariennalibrary.cyberglider;


import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.lariennalibrary.cyberglider.CameraHelper;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.utils.Disposable;


public class WorldController extends InputAdapter implements Disposable
{
	private static final String TAG = WorldController.class.getName();
	//public Sprite[] testSprites;
	//public int selectedSprite; 
	int lives;
	int score;
	public Level level;
	public boolean exitgame;
	
	public CameraHelper cameraHelper;
	
	public WorldController ( Game game )
	{ 
		this.game = game;
		init ();
		
	}

	private void init () 
	{ 
		Gdx.input.setInputProcessor(this);
		
		lives = 3;// Constants.LIVES_START;
		exitgame = false;
		
		initLevel();
		
		//initTestObjects();
	}
	
	private void initLevel ()
	{
		cameraHelper = new CameraHelper();
		timeLeftGameOverDelay = 0;
		gliderdestroyed=false;
		score = 0;
		
		level = new Level ( Constants.LEVEL_01 );
		cameraHelper.setTarget(level.glider);
	}

	/*private void initTestObjects() {
	    // Create new array for 5 sprites
	    testSprites = new Sprite[5];
	    
	 // Create a list of texture regions
	    Array<TextureRegion> regions = new Array<TextureRegion>();
	    regions.add(Assets.instance.glider.glider);
	    regions.add(Assets.instance.data.data);
	    regions.add(Assets.instance.spike.spike);
	    
	 // Create new sprites using a random texture region
	    for (int i = 0; i < testSprites.length; i++) {
	      Sprite spr = new Sprite(regions.random());
	      // Define sprite size to be 1m x 1m in game world
	      spr.setSize(1, 1);
	      // Set origin to sprite's center
	      spr.setOrigin(spr.getWidth() / 2.0f,spr.getHeight() / 2.0f);
	      // Calculate random position for sprite
	      float randomX = MathUtils.random(-2.0f, 2.0f);
	      float randomY = MathUtils.random(-2.0f, 2.0f);
	      spr.setPosition(randomX, randomY);
	      // Put new sprite into array
	      testSprites[i] = spr;
	    }
	    // Set first sprite as selected one
	    selectedSprite = 0;
	   */ 
	    //---------- Old Code ----------
	    
	    // Create empty POT-sized Pixmap with 8 bit RGBA pixel data
	    /*int width = 32;
	    int height = 32;
	    Pixmap pixmap = createProceduralPixmap(width, height);
	    // Create a new texture from pixmap data
	    Texture texture = new Texture(pixmap);
	    // Create new sprites using the just created texture
	    for (int i = 0; i < testSprites.length; i++) 
	    {
	      Sprite spr = new Sprite(texture);
	      // Define sprite size to be 1m x 1m in game world
	      spr.setSize(1, 1);
	      // Set origin to sprite's center
	      spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
	      // Calculate random position for sprite
	      float randomX = MathUtils.random(-2.0f, 2.0f);
	      float randomY = MathUtils.random(-2.0f, 2.0f);
	      spr.setPosition(randomX, randomY);
	      // Put new sprite into array
	      testSprites[i] = spr;
	    }
	    // Set first sprite as selected one
	    selectedSprite = 0;*/
	  //}
	
	private Pixmap createProceduralPixmap (int width, int height) 
	{
	    Pixmap pixmap = new Pixmap(width, height, Format.RGBA8888);
	    // Fill square with red color at 50% opacity
	    pixmap.setColor(1, 0, 0, 0.5f);
	    pixmap.fill();
	    // Draw a yellow-colored X shape on square
	    pixmap.setColor(1, 1, 0, 1);
	    pixmap.drawLine(0, 0, width, height);
	    pixmap.drawLine(width, 0, 0, height);
	    // Draw a cyan-colored border around square
	    pixmap.setColor(0, 1, 1, 1);
	    pixmap.drawRectangle(0, 0, width, height);
	    return pixmap;
	  }
	
	public void update (float deltaTime) 
	{ 
		handleDebugInput(deltaTime);
		if (gliderdestroyed == true) 
		{
		    timeLeftGameOverDelay -= deltaTime;
		    if (timeLeftGameOverDelay <= 0)
		    {
		    	if ( isGameOver() == true)
		    	   backToMenu();
		    	else
		    	   //init();
		    	initLevel();
		    }
		} 
		else 
		{
			handleInputGame(deltaTime);
		}
		
		if (gliderdestroyed == true)
		  {
			  level.glider.velocity.set( 0, 0);
			  level.glider.acceleration.set( 0, 0 );
		  }
		//updateTestObjects (deltaTime);
		level.update(deltaTime);
		testCollisions ();
		cameraHelper.update(deltaTime);
		if (gliderdestroyed == true) 
		{
			if ( timeLeftGameOverDelay == 0)
			{	
				lives--;
		    	
				timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_OVER;
			
				/* gliderdestroyed = false;*/
			}
		    //else
		  //initLevel();
		}
		
		if ( level.glider.position.y >= (100 ) )
		{
			exitgame = true;
		}
	}
	
	/*private void updateTestObjects(float deltaTime) 
	{
	    // Get current rotation from selected sprite
	    float rotation = testSprites[selectedSprite].getRotation();
	    // Rotate sprite by 90 degrees per second
	    rotation += 90 * deltaTime;
	    // Wrap around at 360 degrees
	    rotation %= 360;
	    // Set new rotation value to selected sprite
	    testSprites[selectedSprite].setRotation(rotation);
	  }*/
	
	private void handleDebugInput (float deltaTime) 
	{
	    if (Gdx.app.getType() != Application.ApplicationType.Desktop) return;

	    // Selected Sprite Controls
	    /*float sprMoveSpeed = 5 * deltaTime;
	    if (Gdx.input.isKeyPressed(Keys.A)) moveSelectedSprite(-sprMoveSpeed, 0);
	    if (Gdx.input.isKeyPressed(Keys.D)) moveSelectedSprite(sprMoveSpeed, 0);
	    if (Gdx.input.isKeyPressed(Keys.W)) moveSelectedSprite(0, sprMoveSpeed);
	    if (Gdx.input.isKeyPressed(Keys.S)) moveSelectedSprite(0, -sprMoveSpeed);
	    */
	    
	    
	    if (!cameraHelper.hasTarget(level.glider)) 
	    {
	    // Camera Controls (move)
	    	float camMoveSpeed = 5 * deltaTime;
	    	float camMoveSpeedAccelerationFactor = 5;
	    	if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) camMoveSpeed *= camMoveSpeedAccelerationFactor;
	    //if (Gdx.input.isKeyPressed(Keys.LEFT)) moveCamera(-camMoveSpeed, 0);
	    //if (Gdx.input.isKeyPressed(Keys.RIGHT)) moveCamera(camMoveSpeed, 0);
	    	if (Gdx.input.isKeyPressed(Keys.UP)) moveCamera(0, camMoveSpeed);
	    	if (Gdx.input.isKeyPressed(Keys.DOWN)) moveCamera(0, -camMoveSpeed);
	    //if (Gdx.input.isKeyPressed(Keys.BACKSPACE)) cameraHelper.setPosition(0, 0);

	    // Camera Controls (zoom)
	    	float camZoomSpeed = 1 * deltaTime;
	    	float camZoomSpeedAccelerationFactor = 5;
	    	if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) camZoomSpeed *= camZoomSpeedAccelerationFactor;
	    	if (Gdx.input.isKeyPressed(Keys.COMMA)) cameraHelper.addZoom(camZoomSpeed);
	    	if (Gdx.input.isKeyPressed(Keys.PERIOD)) cameraHelper.addZoom(-camZoomSpeed);
	    	if (Gdx.input.isKeyPressed(Keys.SLASH)) cameraHelper.setZoom(1);
	    }	
	  }

  	  private void moveCamera (float x, float y) {
	    x += cameraHelper.getPosition().x;
	    y += cameraHelper.getPosition().y;
	    cameraHelper.setPosition(x, y);
	  }

	
	  /*private void moveSelectedSprite (float x, float y) 
	  {
	    testSprites[selectedSprite].translate(x, y);
	  }*/
	  
	  @Override
	  public boolean keyUp (int keycode) 
	  {
	    // Reset game world
	    if (keycode == Keys.R) 
	    {
	      init();
	      Gdx.app.debug(TAG, "Game world resetted");
	    }
	    // Select next sprite
	    /*else if (keycode == Keys.SPACE) 
	    {
	      selectedSprite = (selectedSprite + 1) % testSprites.length;
	      
	      // Update camera's target to follow the currently
	      // selected sprite
	      
	      if (cameraHelper.hasTarget()) 
	      {
	        cameraHelper.setTarget(testSprites[selectedSprite]);
	      }
	      Gdx.app.debug(TAG, "Sprite #" + selectedSprite + " selected");
	    }*/
	    
	    
	    // Toggle camera follow
	    else if (keycode == Keys.ENTER) 
	    {
	    	cameraHelper.setTarget(cameraHelper.hasTarget()? null: level.glider);
	    	Gdx.app.debug(TAG, "Camera follow enabled: "+ cameraHelper.hasTarget());
	    }
	 // Back to Menu
	    else if (keycode == Keys.ESCAPE || keycode == Keys.BACK) 
	    {
	      backToMenu();
	    }  
    /*
	    // Toggle camera follow
	    else if (keycode == Keys.ENTER) 
	    {
	      cameraHelper.setTarget(cameraHelper.hasTarget() ? null : testSprites[selectedSprite]);
	      Gdx.app.debug(TAG, "Camera follow enabled: " + cameraHelper.hasTarget());
	    }*/
	    return false;
	  }
	  
	  // Rectangles for collision detection
	  private Rectangle r1 = new Rectangle();
	  private Rectangle r2 = new Rectangle();

	  private void onCollisionGliderWithSpike(Spike spike) 
	  {
		  gliderdestroyed = true;
	  }
	  
	  private void onCollisionGliderWithDataCoin(DataCoin datacoin) 
	  {
		  datacoin.collected = true;
		  score += datacoin.getScore();
		  //Gdx.app.log(TAG, "Data coin collected");
	  };
	  

	  private void testCollisions () 
	  {
	    r1.set(level.glider.bounds.x + level.glider.position.x,
	    		level.glider.bounds.y + level.glider.position.y,
	    		level.glider.bounds.width,level.glider.bounds.height);

	    // Test collision: glider <-> spike
	    for (Spike spike : level.spikes) 
	    {
	      r2.set(spike.bounds.x + spike.position.x, 
	    		  spike.bounds.y + spike.position.y, 
	    		  spike.bounds.width, spike.bounds.height);
	      if ( r1.overlaps(r2) == true)
	         onCollisionGliderWithSpike(spike);
	      
	    }

	    // Test collision: Glider <-> Data Coins
	    for (DataCoin datacoin : level.datacoins) 
	    {
	      if (datacoin.collected == false )
	      { 	  
	         r2.set(datacoin.bounds.x + datacoin.position.x , 
	        		 datacoin.bounds.y + datacoin.position.y,
	        		 datacoin.bounds.width, datacoin.bounds.height);
	      
	         if ( r1.overlaps(r2) == true )
	         {	 
	             onCollisionGliderWithDataCoin(datacoin);
	         }
	      }
	    }

	    
	  }	  
	  
	  private void handleInputGame (float deltaTime) 
	  {
		  if (cameraHelper.hasTarget(level.glider)) 
		  {
			  
			  
			  level.glider.velocity.y = 2.0f;  
		    // Player Movement
		    if (Gdx.input.isKeyPressed(Keys.LEFT)) 
		    {
		      //level.glider.velocity.x =-(level.glider.terminalVelocity.y );
		      level.glider.acceleration.x = -4.0f;
		      //Gdx.app.log(TAG, "LEFT: Acceleration = " + level.glider.acceleration.x );
		    } 
		    else if (Gdx.input.isKeyPressed(Keys.RIGHT)) 
		    {
		        //level.glider.velocity.x = (level.glider.terminalVelocity.y );
		        level.glider.acceleration.x = 4.0f;
		        //Gdx.app.log(TAG, "RIGHT: Acceleration = " + level.glider.acceleration.x );
		    } 
		    else 
		    {
		      // Execute auto-forward movement on non-desktop platform
		      
		        
		        level.glider.acceleration.x = 0;
		      
		    }
		    
		    if ( Gdx.input.isButtonPressed( Input.Buttons.LEFT )
		    		|| Gdx.input.isTouched() )
		    {
		    	if ( Gdx.input.getX() < 240)
		    	{
		    		level.glider.acceleration.x = -4.0f;
		    	}
		    	else if ( Gdx.input.getX() > 240)
		    	{
		    		level.glider.acceleration.x = 4.0f;
		    	}
		    		
		    }
		    
		  }
		  
		  
		}	  
	  
	  private float timeLeftGameOverDelay;
	  private boolean gliderdestroyed;

	  public boolean isGameOver () 
	  {
		  Gdx.app.log("isGameOver", "Passed here" );
		 if (lives <= 0)
			return true;
		 else
	        return false;
	  }
	  
	  private Game game;

	  private void backToMenu () 
	  {
		  exitgame = true;
	    // switch to menu screen
		//  Gdx.app.log("backToMenu", "Before Set Screen" );  
	    //game.setScreen(new MenuScreen(game));
	    //Gdx.app.log("backToMenu", "After Set Screen" );
	  }

	  public void dispose ()
	  {
		  
	  }
	  
}
