# Cyber Glider Demo
by Eric Pietrocupo

This is a small demonstration video game I made a long time ago while reading:

Andras Oehlke.- Learning Libgdx game Development.- Packt publishing.-2013.- ISBN 9781782166047

I don't consider it to be a complete game, it's more use as a libgex example.

# Building the software

## Requirements

* Libgdx: This is the video game library used by the project
* Gradle: The build tool used by Libgdx.

## Building via Gradle

The installation is done via Gradle, so everything should be downloaded for you. This is how libgdx projects are setup. To build the project, simply type:

~~~
gradle desktop:build
~~~

If there was no error, you can run the software with:

~~~
gradle desktop:run
~~~

## Android

I am not sure the android version actually works, I could not build it on my computer probably because I dont have the sdk anymore. This is why I disabled the android sub project One thing I found is that android gralde tool version is linked to the gradle version installed on your computer, so you might need to change the version on this line of code in `build.gradle`:

~~~
    dependencies {
        classpath 'com.android.tools.build:gradle:2.1.3'
    }
~~~
SOURCE: https://stackoverflow.com/questions/57018034/error-gradle-version-2-2-is-required-current-version-is-5-1-1

I disabled the android sub project. You will need to re-enable it by modifying `settings.gradle`, replace the line of code with:

~~~
include 'desktop', 'android', 'core'
~~~

To include android. Then in `build.gradle` you need to uncomment the `:android` block and the android tool line indicated above.

