package com.lariennalibrary.cyberglider.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.lariennalibrary.cyberglider.CyberGlider;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Cyber Glider";
	    //config.useGL20 = false;
	    config.width = 480;
	    config.height = 800;
		new LwjglApplication(new CyberGlider(), config);
	}
}
